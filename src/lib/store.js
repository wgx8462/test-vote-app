import { configureStore } from '@reduxjs/toolkit';  //import 에서 {} 스코프 처리는 내가 원하는 함수만 빼오겠다. 라는 뜻이고
import voteReducer from './features/vote/voteSlice'; //import 에서 그냥 이름으로 처리하면 기본값 주는걸 이 이름으로 사용하겠다 라는 뜻이다.

export default configureStore({
    reducer: {
        vote: voteReducer,
    },
});