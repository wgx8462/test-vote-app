import { createSlice } from '@reduxjs/toolkit';

const initialState = { // init 초기화 초기값 스테이트
    students: ['김길동', '홍길동', '나길동'],
    questions: [
        { kind: 'GOOD', title: '책상이 가장 깨끗한 학생은?' },
        { kind: 'BAD', title: '책상이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '주변이 가장 깨끗한 학생은?' },
        { kind: 'BAD', title: '옷이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '가방이 가장 깨끗한 학생은?' }
    ],
    votes: {},
    currentQuestionIndex: 0,
    selections: {},
};

const voteSlice = createSlice({ // 크리에이트 슬라이스의 반환값은 객체이다.
    name: 'vote',
    initialState,
    reducers: { //액션즈
        select: (state, action) => { // 액션
            const { student, question } = action.payload;
            if (!state.selections[student]) {
                state.selections[student] = { GOOD: [], BAD: [] };
            }
            state.selections[student][question.kind].push(question);
        },
        nextQuestion: (state) => { // 액션
            for (const student in state.selections) {
                if (!state.votes[student]) {
                    state.votes[student] = { GOOD: 0, BAD: 0 };
                }
                state.votes[student].GOOD += state.selections[student].GOOD.length;
                state.votes[student].BAD += state.selections[student].BAD.length;
            }
            state.selections = {};
            state.currentQuestionIndex++;
        },
    },
});

// voteSlice.actions 액션은 reducers 이다.
export const { select, nextQuestion } = voteSlice.actions;

// voteSlice.reducer 리듀서  createSlice 객체 안에 있는 reducer 이다.
export default voteSlice.reducer;